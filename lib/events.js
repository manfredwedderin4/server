const debug = require('@coboxcoop/logger')('@coboxcoop/server:events')
const merge = require('merge-stream')
const { isFunction } = require('util')

class EventStore {
  constructor (sources, opts = {}) {
    this.sources = sources
    this.opts = opts
  }

  createReadStream () {
    const stream = merge()
    this.sources.forEach(addSource)
    return stream

    function addSource (source) {
      source.on('entry', onEntry)
      source.all((err, entries) => {
        if (err) debug({ msg: 'error getting repository entries', err })
        else entries.map(onEntry)
      })
      function onEntry (entry) {
        if (isFunction(entry.createConnectionsStream)) stream.add(entry.createConnectionsStream())
        if (isFunction(entry.createLogStream)) stream.add(entry.createLogStream())
      }
    }
  }
}

module.exports = (sources) => new EventStore(sources)
