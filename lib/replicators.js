const path = require('path')
const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const Repository = require('@coboxcoop/repository')
const Replicator = require('../app/models/replicator')
const { isDevelopment } = require('../util')
const { logResource } = require('./util')

const NAMESPACE = 'replicators'

module.exports = (storage, opts = {}) => {
  const namespace = path.join(storage, NAMESPACE)
  const createReplicator = Replicator(namespace, opts)
  const repository = Repository(namespace, createReplicator)

  if (isDevelopment()) repository.on('entry', logResource)

  return repository
}
