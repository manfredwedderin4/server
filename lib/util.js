const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const SpaceDecorator = require('./decorators/space')

function logResource (entry) {
  var decorator = SpaceDecorator(entry)
  var resource = decorator.toJSON({ secure: true })
  debug({ msg: 'on entry', type: entry.constructor.name, ...resource })
}

module.exports = {
  logResource
}
