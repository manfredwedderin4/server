const thunky = require('thunky')
const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const shutdown = require('http-shutdown')

module.exports = function start (app, opts = {}) {
  return thunky(function (cb = noop) {
    const session = app.get('session')
    session.start((err) => {
      if (err) {
        debug({ msg: 'error on startup', err })
        return cb(err)
      }
      app.set('hostname', opts.hostname)
      app.set('port', opts.port)
      app.set('udpPort', opts.udpPort)
      app.set('storage', opts.storage)
      app.set('mount', opts.mount)

      app.server = app.listen(opts.port, opts.hostname, cb)

      shutdown(app.server)
      return cb()
    })
  })
}

function noop () {}
