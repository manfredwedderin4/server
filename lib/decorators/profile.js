const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const { hex, removeEmpty } = require('../../util')

const ProfileDecorator = module.exports = (profile) => ({
  toJSON: () => removeEmpty({
    name: profile.name,
    publicKey: hex(profile.publicKey),
    id: profile.id
  })
})
