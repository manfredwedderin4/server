const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const { hex, removeEmpty } = require('../../util')
const { loadKey } = require('@coboxcoop/keys')

const SpaceDecorator = module.exports = (space) => ({
  toJSON: (opts = {}) => removeEmpty({
    name: space.name,
    address: hex(space.address),
    discoveryKey: opts.secure ? null : space.discoveryKey ? hex(space.discoveryKey) : null,
    encryptionKey: opts.secure ? hex(loadKey(space.path, 'encryption_key')) : null,
    location: opts.secure ? null : space.drive ? space.drive.location : null,
    size: opts.secure ? null : space.bytesUsed(),
    swarming: space.isSwarming()
  }),
  toPDF: () => ({
    name: `***${space.constructor.name}***: ${space.name}`,
    data: hex(loadKey(space.path, 'encryption_key')),
    comment: `***Address***: ${hex(space.address)}`
  })
})
