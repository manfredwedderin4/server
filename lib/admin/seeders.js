const crypto = require('@coboxcoop/crypto')
const path = require('path')
const Repository = require('@coboxcoop/repository')
const AdminSpace = require('../../app/models/admin/seeder')
const { deriveFromStoredParent } = require('@coboxcoop/keys')
const { isDevelopment } = require('../../util')
const { logResource } = require('../util')
const { assign } = Object

const NAMESPACE = 'seeders'

module.exports = (storage, opts = {}) => {
  const namespace = path.join(storage, NAMESPACE)
  const deriveKeyPair = deriveFromStoredParent(storage, crypto.keyPair)
  const createAdminSpace = AdminSpace(namespace, { deriveKeyPair, ...opts })
  const repository = Repository(namespace, createAdminSpace)

  if (isDevelopment()) repository.on('entry', logResource)

  return repository
}
