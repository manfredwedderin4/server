const path = require('path')
const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const { loadKey, saveKey } = require('@coboxcoop/keys')
const crypto = require('@coboxcoop/crypto')
const CoboxNetworker = require('@coboxcoop/replicator/network')
const Corestore = require('corestore')
const mkdirp = require('mkdirp')
const assert = require('assert')
const { promisify } = require('util')

const AdminSeederStore = require('./admin/seeders')
const ReplicatorStore = require('./replicators')
const SeederStore = require('./seeders')
const EventStore = require('./events')
const Profile = require('../app/models/profile')
const SpaceStore = require('./spaces')

const PARENT_KEY = 'parent_key'
const PROFILES = 'profiles'

class Session {
  constructor (settings, id, opts = {}) {
    assert(settings.storage, 'Session: storage required')
    assert(settings.udpPort, 'Session: udpPort required')
    assert(typeof id === 'number' || typeof id === 'string', 'Session: id required')
    this.settings = settings
    this.id = id
    this.storage = settings.sessionStorage = path.join(settings.storage, PROFILES, id.toString())

    mkdirp.sync(this.storage)
    mkdirp.sync(path.join(this.storage, 'tmp'))

    const parentKey = opts.parentKey || loadKey(this.storage, PARENT_KEY) || crypto.masterKey()
    saveKey(this.storage, PARENT_KEY, parentKey)
    const corestore = new Corestore(path.join(this.storage, 'store'), { masterKey: parentKey })
    const network = new CoboxNetworker(corestore)
    network.listen()

    const profile = Profile(this.storage)
    const identity = profile.publicIdentity
    const spaces = SpaceStore(this.storage, { identity, corestore, network })
    const adminSeeders = AdminSeederStore(this.storage, { identity, corestore, network })
    const replicators = ReplicatorStore(this.storage, { identity, corestore, network })

    this.api = {
      corestore,
      network,
      swarm: network.swarm,
      spaces: { store: spaces },
      replicators: { store: replicators },
      admin: { seeders: { store: adminSeeders } },
      seeders: SeederStore({ port: settings.udpPort }),
      events: EventStore([spaces, replicators, adminSeeders]),
      settings,
      profile,
      system: require('../package.json')
    }
  }

  start (cb) {
    let pending = 4
    this.api.corestore.ready(next)
    this.api.spaces.store.ready(next)
    this.api.replicators.store.ready(next)
    this.api.admin.seeders.store.ready(next)

    function next (err) {
      if (err) {
        pending = Infinity
        return cb(err)
      }
      if (--pending === 0) return cb()
    }
  }

  close (cb) {
    let pending = 4

    this.api.corestore.close(next)
    this.api.spaces.store.close(next)
    this.api.replicators.store.close(next)
    this.api.admin.seeders.store.close(next)

    function next (err) {
      if (err) {
        pending = Infinity
        debug({ msg: 'failed to close session', err })
        return cb(err)
      }
      if (!--pending) return cb()
    }
  }
}

module.exports = Session
module.exports.PROFILES = PROFILES
