const thunky = require('thunky')
const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const capture =  require('capture-exit')
const rimraf = require('rimraf')

module.exports = function close (app, opts) {
  return thunky(function (cb) {
    const session = app.get('session')

    session.close((err) => {
      if (err) {
        debug('failed to close', err)
        return cb(err)
      }
      cb(null, done)
    })

    function done () {
      capture.offExit(app.gracefulExit)
      debug({ msg: 'shutting down the server' })
      app.server.shutdown((err) => {
        debug({ msg: 'closed: server', err })
        var mount = app.get('mount')
        rimraf(mount, (err) => {
          debug({ msg: `unmounted ${mount}`, err })
          return process.exit(0)
        })
      })
    }
  })
}
