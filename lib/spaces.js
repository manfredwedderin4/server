const crypto = require('@coboxcoop/crypto')
const path = require('path')
const Repository = require('@coboxcoop/repository')
const Space = require('../app/models/space')
const { deriveFromStoredParent } = require('@coboxcoop/keys')
const { isDevelopment } = require('../util')
const { logResource } = require('./util')
const { assign } = Object

const NAMESPACE = 'spaces'

module.exports = (storage, opts = {}) => {
  const namespace = path.join(storage, NAMESPACE)
  const deriveKeyPair = deriveFromStoredParent(storage, crypto.keyPair)
  const createSpace = Space(namespace, { deriveKeyPair, ...opts })
  const repository = Repository(namespace, createSpace)

  if (isDevelopment()) repository.on('entry', logResource)

  return repository
}
