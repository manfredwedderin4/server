const constants = require('@coboxcoop/constants')
const debug = require('@coboxcoop/logger')('bin/start')
const Client = require('@coboxcoop/client')
const deepEqual = require('deep-equal')
const { buildBaseURL } = require('@coboxcoop/client/util')
const Ora = require('ora')
const prompts = require('prompts')
const path = require('path')
const config = require('@coboxcoop/config')
const os = require('os')
const { merge } = require('lodash')
const { getDefaultOpts } = require('@coboxcoop/cli/bin/lib/util')
const { untilde, printAppInfo } = require('../util')
const configureFuse = require('./setup-fuse')
const launch = require('../launch')

exports.command = ['start [options]', 'up']
exports.desc = 'start cobox'
exports.builder = {}
exports.handler = setup

const COBOXRC_PATH = path.join(os.homedir(), '.coboxrc')
const DEFAULT_SPACE_SUFFIX = "'s space"

if (require.main === module) return setup(require('yargs').options(require('./lib/options')).argv)

// setup obeys strict priorities for arguments
// argv DOES NOT overwrite RC file, just acts as a one-time override
// config is built with the following priority:
// first from config argument, then RC file, then global defaults
async function setup (argv) {
  let configPath = argv.config || COBOXRC_PATH

  const spinner = new Ora({
    text: `Loading configuration from ${configPath}...`
  })

  spinner.start()

  // load maybe existing config from argv.config or default RC path
  let cfg = config.load(configPath)
  // all changes will be assigned to the copy
  // existing config takes priority over defaults
  let newCfg = Object.assign({}, getDefaultOpts(), cfg)
  // check if storage is empty, if so, assume we've got a blank config
  if (!cfg.storage) spinner.fail('No configuration found. Lets make one!')
  else spinner.succeed()

  /*
  // TODO: abstract this out to use 'session.js'
  const session = await Session.get(cfg)
  const profile = session.api.profile

  if (!profile.name) {
    const questions = [
      {
        type: null,
        name: 'nickname',
        message: 'How do you want to be known to your peers?'
      }
    ]

    const response = await prompts(questions)
    if (response.nickname) {
      profile.update({ name: response.nickname })
      process.stdout.write(`Hi ${profile.name}, welcome...\n`)
    }
  } else {
    process.stdout.write(`Hi ${profile.name}, welcome back!\n`)
  }
  */

  // prioritise argv.mount, but if not passed and
  // config mount not set, ask user to choose:
  // i.e. they have no RC, so need to select a default
  if (!(argv.mount || cfg.mount)) {
    const response = await prompts([
      {
        type: 'text',
        name: 'mount',
        message: 'Where will cobox be accessible?',
        initial: constants.serverDefaults.mount
      }
    ])
    // overwrite newCfg with selected value
    newCfg.mount = response && response.mount || constants.serverDefaults.mount
  }

  // ensure defaults are set with a lower priority than previous config
  cfg = Object.assign({}, constants.serverDefaults, cfg)
  // check if any values have changed, if so, overwrite old RC file
  if (!deepEqual(newCfg, cfg)) {
    spinner.text = `Saving configuration to ${configPath}`
    spinner.start()
    config.save(configPath, newCfg)
    spinner.succeed()
    cfg = newCfg
  }

  // prioritise args, but use other cfg values
  opts = merge({}, cfg, argv)
  // ensure we have a full path set, for printAppInfo display
  opts.mount = path.resolve(untilde(opts.mount))
  opts.storage = path.resolve(untilde(opts.storage))

  // TODO: make this async
  configureFuse.handler({
    quiet: true,
    mount: opts.mount
  }, async (err) => {
    if (err) return exit(err)

    await startServer()
    printAppInfo(opts)
  })

  function exit (err) {
    if (spinner) spinner.fail(err.message)
    console.error(err || 'setup quit')
    process.exit(1)
  }

  function startServer () {
    return new Promise((resolve, reject) => {
      opts.quiet = true
      launch(opts, (err) => {
        if (err) return reject(err)
        resolve()
      })
    })
  }
}
