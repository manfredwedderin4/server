const fs = require('fs')
const { exec } = require('child_process')
const debug = require('@coboxcoop/logger')
const yargs = require('yargs')
const path = require('path')
const chalk = require('chalk')
const { DEFAULT_COLOUR } = require('@coboxcoop/cli/bin/lib/util')
const options = require('./lib/options')

exports.command = 'setup-fuse [options]'
exports.desc = 'configure fuse'
exports.builder = { mount: options.mount }
exports.handler = configureFUSE

function configureFUSE (argv, cb) {
  if (!cb) cb = () => {}
  const user = process.geteuid()
  const space = process.getgid()
  const mountdir = path.resolve(argv.mount)

  var _onSuccess = (msgs) => {
    if (!argv.quiet) console.log(msgs)
  }

  var _onError = (err) => {
    if (!argv.quiet) console.log(err.message)
    if (cb) return cb(err)
  }

  _onSuccess(`Setting up FUSE`)

  checkLocationExists(mountdir, (err, stat) => {
    if (err) return _onError(err)
    if (stat) {
      _onSuccess(`Desired mount point ${chalk.hex(DEFAULT_COLOUR)(mountdir)} already exists..`)
      _onSuccess(`Assessing permissions...`)

      checkPermissions(stat, (err, can) => {
        if (can) {
          _onSuccess(`Correct permissions set!`)
          return onfinish()
        }
        else return setPermissions(mountdir, onfinish)
      })
    } else {
      // try and create the parent directory...
      exec(`mkdir -p ${path.dirname(mountdir)}`, (err) => {
        if (err) return _onError(err)
        checkLocationExists(path.dirname(mountdir), (err, stat) => {
          if (err) return _onError(err)
          if (stat) {
            checkPermissions(stat, (err, can) => {
              if (can) return exec(`mkdir -p ${mountdir}`, (err) => {
                if (err) return onfinish(new Error(`Could not create the ${chalk.hex(DEFAULT_COLOUR)(mountdir)} directory.`)) // TODO: pass error code
                else return onfinish()
              })

              return exec(`sudo mkdir -p ${mountdir}`, (err) => {
                if (err) return onfinish(new Error(`Could not create the ${chalk.hex(DEFAULT_COLOUR)(mountdir)} directory.`)) // TODO: pass error code
                return setPermissions(mountdir, onfinish)
              })
            })
          }
        })
      })
    }
  })

  function onfinish (err) {
    if (err) return _onError(err)
    _onSuccess(`Mount point set to ${chalk.hex(DEFAULT_COLOUR)(mountdir)}`, `Successfully configured FUSE`)
    _onSuccess(`Successfully configured FUSE`)
    return cb()
  }

  function checkLocationExists (location, cb) {
    fs.stat(location, (err, stat) => {
      if (err && err.errno !== -2) return cb(new Error(`Could not get the status of ${chalk.red(location)}.`))
      else cb(null, stat)
    })
  }

  function checkPermissions (stat, cb) {
    return cb(null, stat.uid === user && stat.gid === space && !!(2 & parseInt((stat.mode & parseInt("777", 8)).toString(8)[0])))
  }

  function setPermissions (location, cb) {
    exec(`sudo chown ${user}:${space} ${location}`, (err) => {
      if (err) return cb(new Error(`Could not change the permissions on the ${chalk.red(location)} directory.`))
      return cb()
    })
  }
}
