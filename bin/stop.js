const Client = require('@coboxcoop/client')
const { buildBaseURL } = require('@coboxcoop/client/util')
const { getDefaultOpts, handleAjaxErrors } = require('@coboxcoop/cli/bin/lib/util')

exports.command = ['stop [options]', 'down']
exports.desc = 'stop the app'
exports.builder = getDefaultOpts()
exports.handler = stop

async function stop (argv) {
  const client = new Client({ endpoint: buildBaseURL(argv) })
  try {
    await client.get('stop')
    console.log('cobox closed successfully')
  } catch (err) {
    return handleAjaxErrors(err)
  }
}
