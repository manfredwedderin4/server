const { describe } = require('tape-plus')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const Repository = require('@coboxcoop/repository')
const proxyquire = require('proxyquire')
const { assign, values } = Object

const seedResources = require('../../../../../fixtures/resource')
const stubResource = require('../../../../../stubs/resource')
const stubProfile = require('../../../../../stubs/profile')
const BroadcastCommandsController = require('../../../../../../app/controllers/admin/seeders/commands/broadcasts')

const { hex } = require('../../../../../../util')

describe('Controllers: admin/seeders/commands', (context) => {
  let api, stubs, seeds, controller, seed

  context.beforeEach((c) => {
    stubs = {
      ready: sinon.stub().callsArg(0),
      encryptionKey: hex(crypto.randomBytes(32)),
      log: { publish: sinon.stub().resolves(true) }
    }

    api = {
      admin: { seeders: { store: Repository(null, stubResource(null, stubs)) } },
      profile: stubProfile()
    }

    seeds = seedResources()
    api.admin.seeders.store._seed(seeds)
    seed = values(seeds)[values(seeds).length - 1]
    record = api.admin.seeders.store.collection[seed.address]

    address = hex(crypto.address())
    seedersStub = { show: sinon.stub().resolves(record) }
    controller = new BroadcastCommandsController(api, seedersStub)
  })

  context('create a commands/announce record', async (assert, next) => {
    await controller.create({ address }, seed)
    assert.ok(stubs.log.publish.calledOnce, 'calls publish on space log')
    next()
  })

  context('create a commands/hide record', async (assert, next) => {
    const seed = seeds[seeds.length - 1]
    await controller.destroy({ address }, seed)
    assert.ok(stubs.log.publish.calledOnce, 'calls publish on space log')
    next()
  })
})
