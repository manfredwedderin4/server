const { describe } = require('tape-plus')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const Repository = require('@coboxcoop/repository')
const randomWords = require('random-words')
const omit = require('lodash.omit')
const { values } = Object

const AdminSeedersController = require('../../../../app/controllers/admin/seeders')
const seedResources = require('../../../fixtures/resource')
const stubResource = require('../../../stubs/resource')
const stubProfile = require('../../../stubs/profile')

const { hex } = require('../../../../util')

describe('Controllers: admin/seeders', (context) => {
  let api, stubs, seeds, controller

  context.beforeEach((c) => {
    stubs = {
      ready: sinon.stub().callsArg(0),
      log: { publish: sinon.stub().resolves() },
      swarm: sinon.stub()
    }

    api = {
      admin: { seeders: { store: Repository(null, stubResource(null, stubs)) } },
      seeders: { store: sinon.stub() },
      profile: stubProfile()
    }

    seeds = seedResources()
    api.admin.seeders.store._seed(seeds)
    api.admin.seeders.store.isReady = true
    entries = values(seeds)
    controller = new AdminSeedersController(api)
  })

  context('index: lists all admin seeders', async (assert, next) => {
    const seeders = await controller.index(null, { decorate: true })
    assert.same(seeders, entries, 'gets all seeders')
    next()
  })

  context('create: creates a new seeder', async (assert, next) => {
    const params = {
      name: randomWords(1).pop(),
      address: hex(crypto.address()),
      encryptionKey: hex(crypto.randomBytes(32))
    }

    const seeder = await controller.create(params, { decorate: true })
    const check = omit(params, ['encryptionKey'])
    assert.same(seeder, check, 'creates a seeder')
    next()
  })

  // TODO
  context('create:  from an invite code', async (assert, next) => {
    next()
  })

  context('create: processes nested commands', async (assert, next) => {
    const commands = [
      { action: 'announce' },
      { action: 'invalid-command' },
      { action: 'replicate', name: 'magma', address: hex(crypto.address()) }
    ]

    const params = {
      name: randomWords(1).pop(),
      address: hex(crypto.address()),
      encryptionKey: hex(crypto.randomBytes(32)),
      commands
    }

    const seeder = await controller.create(params, { decorate: true })
    assert.same(stubs.log.publish.callCount, 4, 'publishes 4 messages')
    next()
  })

  context('show: gets a seeder', async (assert, next) => {
    const seed = values(seeds)[values(seeds).length - 1]
    const seeder = await controller.show(seed, { decorate: true })
    assert.same(seeder, seed, 'shows a seeder')
    next()
  })
})
