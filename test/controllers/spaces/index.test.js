const { describe } = require('tape-plus')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const Repository = require('@coboxcoop/repository')
const randomWords = require('random-words')
const { values } = Object

const seedResources = require('../../fixtures/resource')
const stubResource = require('../../stubs/resource')
const stubProfile = require('../../stubs/profile')
const SpacesController = require('../../../app/controllers/spaces')
const { hex } = require('../../../util')

describe('Controllers: spaces', (context) => {
  let api, stubs, seeds, controller, entries

  context.beforeEach((c) => {
    stubs = {
      ready: sinon.stub().callsArg(0),
      log: { publish: sinon.stub().resolves() }
    }
    api = {
      spaces: { store: Repository(null, stubResource(null, stubs)) },
      profile: stubProfile()
    }
    seeds = seedResources()
    api.spaces.store._seed(seeds)
    entries = values(seeds)
    controller = new SpacesController(api)
  })

  context('index: lists all spaces', async (assert, next) => {
    const spaces= await controller.index(null, { decorate: true })
    assert.same(spaces, entries, 'gets all spaces')
    next()
  })

  context('create: creates a new space', async (assert, next) => {
    const params = {
      name: randomWords(1).pop(),
      address: hex(crypto.address())
    }

    const space = await controller.create(params, { decorate: true })
    assert.same(space, params, 'creates a space')
    assert.ok(stubs.log.publish.calledTwice, 'publishes two messages')
    next()
  })

  context('show: finds a space', async (assert, next) => {
    const seed = entries[entries.length - 1]
    const space = await controller.show(seed, { decorate: true })
    assert.same(space, seed, 'shows a space')
    next()
  })

  context('destroy: removes a space', async (assert, next) => {
    const seed = entries[entries.length - 1]
    await controller.destroy(seed, { decorate: true })

    const list = await controller.index(null, { decorate: true })
    assert.same(list.length, entries.length - 1, 'one fewer spaces')
    assert.same(list.filter((entry) => entry.address === seed.address).length, 0, 'space gone')
    next()
  })
})

