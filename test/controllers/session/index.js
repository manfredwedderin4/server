const { describe, beforeAll } = require('tape-plus')
const { tmp, cleanup } = require('../../util')
const Application = require('../../../index.js')

const storage = tmp()
var settings = { storage, udpPort: 8112 }
const app = Application(settings)
app.start((err) => {
  describe('sessions', (context) => {
    context('list', async (assert, next) => {
      var sessions = await app.sessions.list()
      assert.same(sessions.length, 1, 'gets all sessions')
      next()
    })

    context('create', async (assert, next) => {
      let opts = { storage }
      var session = await app.sessions.create()
      assert.same(session, 1, 'creates new session')
      var session2 = await app.sessions.create()
      assert.same(session2, 2, 'creates new session')
      next()
    })

    /*
    context('export', async (assert, next) => {
      var stream = await controllers.export.index()
      // checksome stuff things here...
      cleanup(storage, next)
    })

    context('import', async (assert, next) => {
      var res = await controllers.import.create()
      // and here...
      cleanup(storage, next)
    })
    */

    context('change', async (assert, next) => {
      var sessions = await app.sessions.list()
      var session = sessions[1]
      app.sessions.change(session.id, (err) => {
        app.close((err, done) => {
          done()
          cleanup(storage, next)
        })
      })
    })
  })
})
