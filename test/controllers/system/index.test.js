const { describe } = require('tape-plus')
const randomWords = require('random-words')
const SystemController = require('../../../app/controllers/system')
const fs = require('fs')
const path = require('path')
const { hex } = require('../../../util')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const system = require('../../../package.json')
const { cleanup, tmp } = require('../../util') 

describe('Controllers: system', (context) => {
  let api, controller

  context.beforeEach((c) => {
    api = {
      system,
      settings: {}
    }
    controller = new SystemController(api)
  })

  context('show: gets system information', async (assert, next) => {
    const data = await controller.show()
    assert.ok(data, 'returns your profile')
    assert.ok(data.name, 'returns the name')
    assert.ok(data.description, 'returns the description')
    assert.ok(data.version, 'returns the version')
    assert.ok(data.author, 'returns the author')
    assert.ok(data.license, 'returns the license')
    assert.ok(data.bugs.url, 'returns the bugs url')
    next()
  })

  context('logs: returns logs path', async (assert, next) => {
    api.settings.logsPath = tmp()
    fs.writeFileSync(path.join(api.settings.logsPath, 'main.log'), 'testing')
    const stream = await controller.logs()
    stream.on('data', (chunk) => {
      assert.ok(chunk, Buffer.from('testing'), 'returns the logs path')
      cleanup(api.settings.logsPath, next)
    })
  })
})
