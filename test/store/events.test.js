const { describe } = require('tape-plus')
const path = require('path')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const Corestore = require('corestore')
const CoboxNetworker = require('@coboxcoop/replicator/network')
const Repository = require('@coboxcoop/repository')
const Space = require('../../app/models/space')
const Replicator = require('../../app/models/replicator')
const AdminSeeder = require('../../app/models/admin/seeder')
const randomWords = require('random-words')
const Profile = require('../stubs/profile')

const SpaceStore = require('../../lib/spaces')
const AdminSeederStore = require('../../lib/admin/seeders')
const ReplicatorStore = require('../../lib/replicators')

const AdminController = require('../../app/controllers/admin')
const EventsController = require('../../app/controllers/events')
const SpacesController = require('../../app/controllers/spaces')
const ReplicatorsController = require('../../app/controllers/replicators')
const ProfileController = require('../../app/controllers/profile')

const AdminValidator = require('../../app/validators/admin')
const SpacesValidator = require('../../app/validators/spaces')
const InviteValidator = require('../../app/validators/invites')
const ReplicatorsValidator = require('../../app/validators/replicators')
const ProfileValidator = require('../../app/validators/profile')
const SeederStore = require('../mocks/seeder-store')

const { hex } = require('../../util')
const EventStore = require('../../lib/events')
const { tmp, cleanup } = require('../util')

describe('EventStore', (context) => {
  let api, storage, app

  context.beforeEach((c) => {
    storage = tmp()

    const corestore = new Corestore(path.join(storage, 'store'))
    const network = new CoboxNetworker(corestore)
    const identity = Profile()

    const spaces = SpaceStore(storage, { corestore, network, identity })
    const adminSeeders = AdminSeederStore(storage, { corestore, network, identity })
    const replicators = ReplicatorStore(storage, { corestore, network, identity })

    api = {
      spaces: { store: spaces },
      admin: { seeders: { store: adminSeeders } },
      replicators: { store: replicators },
      seeders: SeederStore(),
      profile: identity
    }

    app = {
      controllers: {
        admin: new AdminController(api),
        events: new EventsController(api),
        spaces: new SpacesController(api),
        profile: new ProfileController(api),
        replicators: new ReplicatorsController(api)
      },
      validators: {
        admin: AdminValidator(api),
        spaces: SpacesValidator(api),
        invites: InviteValidator(api),
        profile: ProfileValidator(api),
        replicators: ReplicatorsValidator(api)
      }
    }
  })

  context('live stream', async (assert, next) => {
    let count = 0

    let data = [
      { resourceType: 'SPACE', data: { type: 'peer/about' } },
      { resourceType: 'SPACE', data: { type: 'space/about' } },
      { resourceType: 'SPACE', data: { type: 'peer/invite' } },
      { resourceType: 'ADMIN_SEEDER', data: { type: 'peer/about' } },
      { resourceType: 'ADMIN_SEEDER', data: { type: 'space/about' } },
      { resourceType: 'ADMIN_SEEDER', data: { type: 'peer/invite' } }
    ]

    const sources = [api.spaces.store, api.replicators.store, api.admin.seeders.store]
    const events = EventStore(sources)
    const stream = events.createReadStream()

    stream.on('data', check)

    // create a new space
    var space = await app.controllers.spaces.create({ name: randomWords(1).pop() }, { decorate: true })

    setTimeout(async () => {
      // create a new space invite
      var publicKey = hex(crypto.boxKeyPair().publicKey)
      await app.controllers.spaces.invites.create({ publicKey }, { address: hex(space.address) })

      setTimeout(async () => {
        // create a new admin seeder
        var seeder = await app.controllers.admin.seeders.create({
          name: randomWords(1).pop(),
          address: hex(crypto.address()),
          encryptionKey: hex(crypto.randomBytes(32))
        }, { decorate: true })

        setTimeout(async () => {
          // create a new admin seeder invite
          await app.controllers.admin.seeders.invites.create({ publicKey }, { address: hex(seeder.address) })
        }, 10)
      }, 10)
    }, 10)

    function check (msg) {
      if (msg.sync) return
      let check = data[count]
      assert.same(check.resourceType, msg.resourceType, 'correct resourceType')
      assert.same(check.data.type, msg.data.type, 'correct msg type')
      ++count
      done()
    }

    function done () {
      if (count === data.length) {
        stream.destroy()
        return next()
        return cleanup([storage], next)
      }
    }
  })
})
