const path = require('path')
const fs = require('fs')
const request = require('supertest')
const { describe } = require('tape-plus')
const httpMocks = require('node-mocks-http')
const randomWords = require('random-words')
const { tmp, cleanup } = require('../../util')
const SystemController = require('../../../app/controllers/system')
const Router = require('../../../app/routes/system')
const system = require('../../../package.json')

const App = require('../../../index')

const { hex } = require('../../../util')

describe('Router: /system', (context) => {
  let app, api

  context.beforeEach((c) => {
    api = { system, settings: {} }
    app = { controllers: { system: new SystemController(api) } }
  })

  context('GET / - valid with correct parameters', (assert, next) => {
    var request = httpMocks.createRequest({
      method: 'GET',
      url: '/'
    })

    var response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    var routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      var data = response._getJSONData()
      assert.ok(data, 'gives a response')
      assert.same(data.name, system.name , 'returns the name')
      assert.same(data.description, system.description , 'returns the description')
      assert.same(data.version, system.version , 'returns the version')
      assert.same(data.author, system.author , 'returns the author')
      assert.same(data.license, system.license , 'returns the license')
      assert.same(data.bugs.url, system.bugs.url, 'returns the bugs url')
      next()
    })
  })

  context('GET /logs - valid', (assert, next) => {
    api.settings.logsPath = tmp()
    fs.writeFileSync(path.join(api.settings.logsPath, 'main.log'), 'testing')

    let request = httpMocks.createRequest({
      method: 'GET',
      url: `/logs`
    })

    const response = httpMocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })

    const routeHandler = Router(app)
    routeHandler(request, response)

    response.on('end', () => {
      assert.same(response.statusCode, 200, 'returns a 200')
      const data = response._getChunks().map((d) => d.toString('utf8'))[0]
      assert.same(data, 'testing', 'returns file content')
      next()
    })
  })
})
