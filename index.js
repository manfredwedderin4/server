const os = require('os')
const fs = require('fs')
const assert = require('assert')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:index')
const express = require('express')
const expressWs = require('express-ws')
const bodyParser = require('body-parser')
const swaggerUI = require('swagger-ui-express')
const constants = require('@coboxcoop/constants')
const crypto = require('@coboxcoop/crypto')
const path = require('path')
const mkdirp = require('mkdirp')
const cors = require('cors')
const capture = require('capture-exit')
const { promisify } = require('util')
const { assign } = Object
const { requestLogger } = require('@coboxcoop/logger')

capture.captureExit()

const start = require('./lib/start')
const close = require('./lib/close')
const swaggerDocs = require('./docs/swagger.json')
const Routes = require('./app/routes')
const Profile = require('./app/models/profile')
const Session = require('./lib/session')
const { hex, untilde } = require('./util')

const VERSION = require('./version')

function Application (opts = {}) {
  debug({ platform: os.platform(), arch: os.arch() })

  const port = opts.port || 9112
  const udpPort = opts.udpPort || 8999
  const mount = opts.mount && untilde(opts.mount) || constants.mount
  const storage = opts.storage && path.join(untilde(opts.storage), VERSION) || path.join(constants.storage, VERSION)
  const logsPath = path.join(storage, 'logs', 'server')
  const settings = { storage, mount, port, udpPort, logsPath }

  mkdirp.sync(settings.logsPath)
  mkdirp.sync(settings.mount)

  const app = express()
  expressWs(app)
  app.set('_id', hex(crypto.randomBytes(2)))

  app.sessions = new SessionManager(settings, {
    getCurrentSession: () => app.get('session'),
    onChangeSession: (session) => {
      app.set('session', session)
      router = Routes(app)
    }
  })
  const session = app.sessions.get(settings)
  app.set('session', session)

  app.use(express.static(path.join(__dirname, 'public')))
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())
  app.use(cors())
  app.use(requestLogger('@coboxcoop/server:request'))
  app.use(require('./app/middleware/log-request'))

  app.gracefulExit = Shutdown(app)

  var router = Routes(app)
  app.use('/api', (req, res, next) => router(req, res, next))
  app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs))

  var uiPath = path.join(path.dirname(require.resolve('.')), 'public')
  if (!fs.existsSync(path.join(uiPath, 'index.html'))) {
    uiPath = path.join(path.dirname(require.resolve('.')), 'public', 'default')
  }
  app.use(express.static(uiPath))
  app.use('*', UILoaderMiddleware)

  app.start = start(app, assign(opts, settings))
  app.close = close(app, assign(opts, settings))
  capture.onExit(app.gracefulExit)

  function UILoaderMiddleware (req, res, next) {
    if (!req.url.startsWith('/api')) {
      res.statusCode = 200
      var ui = path.join(__dirname, 'public', 'index.html')
      if (fs.existsSync(ui)) {
        return fs.createReadStream(ui).pipe(res)
      } else {
        return fs.createReadStream(path.join(__dirname, 'public', 'default', 'index.html')).pipe(res)
      }
    }
    next()
  }

  function Shutdown (app) {
    return function gracefulShutdown () {
      return new Promise((resolve, reject) => {
        app.close((err, done) => {
          if (err) return reject(err)
          return resolve(done)
        })
      })
    }
  }

  return app
}

module.exports = Application

// TODO HACK XXX: this should actualy be somewhere else, but we can't
// do that because there isn't yet a good handle on dependency injection
class SessionManager {
  constructor (settings, opts) {
    this.sessions = path.join(settings.storage, Session.PROFILES)
    this.settings = settings
    assert(opts.onChangeSession, 'SessionManager: requires onChange function')
    assert(opts.getCurrentSession, 'SessionManager: requires getCurrentSession function')
    this._onChangeSession = opts.onChangeSession
    this._getCurrentSession = opts.getCurrentSession
  }

  list () {
    try {
      var ids = fs.readdirSync(this.sessions)
    } catch (err) {
      return []
    }
    var ordered = ids.sort()
    return ordered.map((id) => {
      var profile = Profile(path.join(this.sessions, id))
      return {
        id,
        name: profile.name,
        publicKey: profile.publicKey
      }
    })
  }

  _getLatestSessionId () {
    var sessions = this.list()
    return sessions.length > 0 ? sessions[sessions.length - 1].id : 0
  }

  get () {
    var id = this._getLatestSessionId()
    return new Session(this.settings, id)
  }

  create (opts) {
    var id = this._getLatestSessionId()
    return new Session(this.settings, parseInt(id) + 1, opts)
  }

  change (id, cb) {
    var session = this._getCurrentSession()
    session.close(err => {
      if (err) return cb(err)
      var availableSessions = this.list()
      var params = availableSessions.find(s => s.id.toString() === id.toString())
      if (!params) return cb(new Error(`session with id ${id} not found`))
      var session = new Session(this.settings, params.id)
      session.start(err => {
        if (err) return cb(err)
        this._onChangeSession(session)
        cb(null, session)
      })
    })
  }
}
