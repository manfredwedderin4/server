const constants = require('@coboxcoop/constants')
const fs = require('fs')
const path = require('path')
const pm2 = require('pm2')
const { pluck, flatten, printAppInfo } = require('./util')

const VERSION = require('./version')

module.exports = function production (opts, cb) {
  const logPath = path.join(path.resolve(opts.storage || constants.storage), VERSION, 'logs', 'server')
  const args = flatten(pluck(opts, ['port', 'hostname', 'storage', 'dev', 'mount', 'udpPort', 'ui']))
  const script = require.resolve('./main.js')

  args.push('--env')
  args.push('production')

  mkdir(logPath)
  ensureLogFiles(logPath)

  pm2.connect((err) => {
    if (err) process.exit(2)

    var pm2opts = {
      name: 'cobox-app',
      script,
      args,
      env: { NODE_ENV: 'production' },
      error_file: path.join(logPath, 'error.log'),
      log_file: path.join(logPath, 'main.log'),
      max_memory_restart: '1000M',
      autorestart: false
    }

    pm2.start(pm2opts, (err, apps) => {
      pm2.disconnect()
      if (err) throw err
      return cb(err, opts)
    })
  })
}

function mkdir (p) {
  try { fs.mkdirSync(p, { recursive: true }) }
  catch (err) { throw err }
}

function ensureLogFiles (p) {
  if (!fs.existsSync(path.join(p, 'main.log'))) fs.closeSync(fs.openSync(path.join(p, 'main.log'), 'a'))
  if (!fs.existsSync(path.join(p, 'error.log'))) fs.closeSync(fs.openSync(path.join(p, 'error.log'), 'a'))
}
