const debug = require('@coboxcoop/logger')('@coboxcoop/server:cli')
const { spawn } = require('child_process')
const { pluck, flatten } = require('./util')

const DEV_DEBUG = '*,-express*,-hypercore-protocol*,-bodyparser*,-babel*'

module.exports = function development (opts, cb) {
  const args = flatten(pluck(opts, ['port', 'hostname', 'storage', 'dev', 'mount', 'udpPort', 'ui']))
  const script = require.resolve('./main.js')

  debug('starting server in developer\'s mode')
  process.env.DEBUG = process.env.DEBUG || DEV_DEBUG
  process.env.NODE_ENV = process.env.NODE_ENV || 'development'

  args.unshift(script)

  spawn(process.execPath, args, {
    env: {
      ...process.env,
      FORCE_COLOR: process.env.FORCE_COLOR || '2'
    },
    stdio: 'inherit'
  })

  return cb(null, opts)
}
