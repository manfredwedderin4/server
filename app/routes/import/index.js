const express = require('express')
const expressWs = require('express-ws')
const toHTTP = require('../../middleware/to-http')

const ImportRoutes = module.exports = ({ controllers, validators, upload }) => {
  const router = express.Router()
  expressWs(router)

  router.post('/', upload.single('export'), toHTTP(controllers.import, 'create'))

  return router
}
