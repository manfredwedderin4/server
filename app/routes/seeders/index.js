const express = require('express')
const expressWs = require('express-ws')
const toWS = require('../../middleware/to-ws')
const toHTTP = require('../../middleware/to-http')

const EventRoutes = module.exports = ({ controllers, validators }) => {
  const router = express.Router()
  expressWs(router)

  router.ws('/', toWS(controllers.seeders, 'live'))
  router.get('/', toHTTP(controllers.seeders,'index'))
  router.get('/:id', toHTTP(controllers.seeders,'show'))

  return router
}

