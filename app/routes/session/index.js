const express = require('express')
const expressWs = require('express-ws')
const toHTTP = require('../../middleware/to-http')

const Routes = module.exports = ({ controllers, validators }) => {
  const router = express.Router()
  expressWs(router)

  router.get('/', toHTTP(controllers.session, 'list'))
  router.put('/:id', toHTTP(controllers.session, 'change'))

  return router
}
