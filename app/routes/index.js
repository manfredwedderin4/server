const express = require('express')
const expressWs = require('express-ws')
const toHTTP = require('../middleware/to-http')
const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const capture = require('capture-exit')
const path = require('path')
const multer = require('multer')

const ApplicationController = require('../controllers')
const ApplicationValidator = require('../validators')

module.exports = function Routes (app) {
  const session = app.get('session')
  const api = session.api
  const router = express.Router()
  const upload = multer({ dest: path.join(api.settings.storage, 'tmp') })
  expressWs(router)

  const server = {
    controllers: new ApplicationController(app),
    validators: ApplicationValidator(api),
    upload
  }

  router.use('/', require('./events')(server))
  router.use('/admin', require('./admin')(server))
  router.use('/seeders', require('./seeders')(server))
  router.use('/spaces', require('./spaces')(server))
  router.use('/profile', require('./profile')(server))
  router.use('/replicators', require('./replicators')(server))
  router.use('/system', require('./system')(server))
  router.use('/export', require('./export')(server))
  router.use('/import', require('./import')(server))
  router.use('/session', require('./session')(server))

  router.get('/system/routes', (req, res) => {
    const routes = server.controllers.system.routes({ router })

    return res
      .status(200)
      .json(routes)
  })

  router.get('/stop', (req, res) => {
    app.gracefulExit()
      .then((done = noop) => {
        res.status(200)
        res.end()
        return process.nextTick(done)
      }).catch((err, done = noop) => {
        debug({ msg: 'failed to exit gracefully', err })
        res.status(500)
        res.end()
        return process.nextTick(done)
      })
  })

  return router
}

function noop () {}
