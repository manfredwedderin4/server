const express = require('express')
const expressWs = require('express-ws')
const toHTTP = require('../../../middleware/to-http')

const AdminSeederRoutes = module.exports = ({ controllers, validators, helpers }) => {
  const router = express.Router()
  expressWs(router)

  router.get('/', toHTTP(controllers.admin.seeders, 'index'))
  router.get('/:id', toHTTP(controllers.admin.seeders, 'show'))
  router.post('/', validators.admin.seeders.create, toHTTP(controllers.admin.seeders, 'create'))

  router.get('/:id/peers', toHTTP(controllers.admin.seeders.peers, 'index'))

  router.get('/:id/connections', toHTTP(controllers.admin.seeders.connections, 'index'))
  router.post('/:id/connections/', toHTTP(controllers.admin.seeders.connections, 'create'))
  router.delete('/:id/connections/', toHTTP(controllers.admin.seeders.connections, 'destroy'))
  router.post('/connections', toHTTP(controllers.admin.seeders.connections, 'createAll'))
  router.delete('/connections', toHTTP(controllers.admin.seeders.connections, 'destroyAll'))

  router.get('/invites/accept', validators.invites.accept, toHTTP(controllers.admin.seeders.invites, 'accept'))
  router.get('/:id/invites', toHTTP(controllers.admin.seeders.invites, 'index'))
  router.post('/:id/invites/', validators.invites.create, toHTTP(controllers.admin.seeders.invites, 'create'))

  // TODO: make these RESTful (POST/DELETE)
  router.patch('/:id/commands/broadcast', toHTTP(controllers.admin.seeders.commands.broadcasts, 'update'))
  router.post('/:id/commands/announce', toHTTP(controllers.admin.seeders.commands.broadcasts, 'create'))
  router.post('/:id/commands/hide', toHTTP(controllers.admin.seeders.commands.broadcasts, 'destroy'))

  // TODO: make these RESTful (POST/DELETE)
  router.get('/:id/commands/replicates', toHTTP(controllers.admin.seeders.commands.replicates, 'index'))
  router.post('/:id/commands/replicate', validators.admin.seeders.commands.replicates.create, toHTTP(controllers.admin.seeders.commands.replicates, 'create'))
  router.post('/:id/commands/unreplicate', validators.admin.seeders.commands.replicates.destroy, toHTTP(controllers.admin.seeders.commands.replicates, 'destroy'))

  router.delete('/:id', toHTTP(controllers.admin.seeders, 'destroy'))

  return router
}

