const express = require('express')
const expressWs = require('express-ws')
const toHTTP = require('../../middleware/to-http')

const KeyRoutes = module.exports = ({ controllers, validators }) => {
  const router = express.Router()
  expressWs(router)

  router.get('/', toHTTP(controllers.system, 'show'))
  router.get('/logs', toHTTP(controllers.system, 'logs',  { stream: true, filename: 'cobox.log' }))

  return router
}
