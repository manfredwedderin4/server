const express = require('express')
const expressWs = require('express-ws')
const toHTTP = require('../../middleware/to-http')

const ExportRoutes = module.exports = ({ controllers, validators }) => {
  const router = express.Router()
  expressWs(router)

  router.get('/', toHTTP(controllers.export, 'index', { stream: true, filename: 'backup.cobox' }))

  return router
}
