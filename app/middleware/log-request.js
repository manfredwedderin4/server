const { isProduction } = require('../../util')

module.exports = function logRequest (req, res, next) {
  if (isProduction()) req.log.info('request')
  next()
}
