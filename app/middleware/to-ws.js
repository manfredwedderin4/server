const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const websocketStream = require('websocket-stream/stream')
const { assign } = Object
const jsonify = require('../helpers/jsonify')

module.exports = function toWS (controller, action) {
  return async function (ws, req) {
    let opts = assign({}, req.query, req.params)

    const method = controller[action].bind(controller)

    debug('controller', controller.constructor.name)
    debug('action', action)
    if (!method) throw new Error('invalid controller method', controller.constructor.name, action, method)
    debug('calling method', method.name.substr(6, method.name.length))

    return method(opts)
      .pipe(jsonify())
      .pipe(websocketStream(ws, {
        objectMode: true
      }))
  }
}
