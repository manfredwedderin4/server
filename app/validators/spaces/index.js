const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const crypto = require('@coboxcoop/crypto')
const { oneOf, body } = require('express-validator')
const assert = require('assert')
const { hex } = require('../../../util')
const { definitions } = require('@coboxcoop/schemas')

const {
  encryptionKeyChain,
  codeChain,
  SpaceByNameChain,
  SpaceByAddressChain
} = require('../chains')

module.exports = (api) => {
  const spaceByNameChain = SpaceByNameChain(api.spaces.store)
  const spaceByAddressChain = SpaceByAddressChain(api.spaces.store)

  return {
    create: oneOf([
      spaceByNameChain(),
      [
        spaceByNameChain(),
        spaceByAddressChain(),
        encryptionKeyChain()
      ],
      [
        codeChain()
      ]
    ]),

    drive: require('./drive')(api),
    mounts: require('./mount')(api)
  }
}
