const { body, oneOf } = require('express-validator')
const assert = require('assert')
const crypto = require('@coboxcoop/crypto')

const {
  encryptionKeyChain,
  nestedCommandsChain,
  publicKeyChain,
  SpaceByNameChain,
  SpaceByAddressChain,
  codeChain
} = require('../../chains')

module.exports = (api) => {
  const adminSeederByNameChain = SpaceByNameChain(api.admin.seeders.store)
  const adminSeederByAddressChain = SpaceByAddressChain(api.admin.seeders.store)

  return {
    create: [
      oneOf([
        [
          adminSeederByNameChain(),
          adminSeederByAddressChain(),
          encryptionKeyChain()
        ],
        [
          publicKeyChain()
        ],
        [
          codeChain()
        ]
      ]),
      nestedCommandsChain()
    ],

    commands: require('./commands')(api)
  }
}
