const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const crypto = require('@coboxcoop/crypto')
const ProfileDecorator = require('../../../lib/decorators/profile')

module.exports = class ProfileController {
  constructor (api) {
    this.profile = api.profile
  }

  // GET /api/profile
  async show (params, opts = {}) {
    var decorator = ProfileDecorator(this.profile)
    return decorator.toJSON()
  }

  // PATCH /api/profile
  async update (params, opts = {}) {
    this.profile.update(params)

    var decorator = ProfileDecorator(this.profile)
    return decorator.toJSON()
  }
}
