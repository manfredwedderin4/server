const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const path = require('path')
const fs = require('fs')
const pick = require('lodash.pick')
const { assign } = Object

class SystemController {
  constructor (api) {
    this.api = api
  }

  // GET /api/system/routes
  routes (params, opts) {
    const router = params.router
    return []
      .concat
      .apply([], router.stack.map((layer) => recurse(layer)))
      .filter(Boolean)
  }

  // GET /api/system/logs
  async logs (params, opts = {}) {
    const logsPath = path.join(this.api.settings.logsPath, 'main.log')
    var stat = fs.statSync(logsPath)
    if (stat) return fs.createReadStream(logsPath)
    else return false
  }

  // GET /api/system
  async show (params, opts = {}) {
    const system = assign({}, systemParams(this.api.system), this.api.settings)
    return system
  }
}

function systemParams (params) {
  return pick(params, [
    'name',
    'description',
    'version',
    'author',
    'license',
    'bugs'
  ])
}

function recurse (layer) {
  if (!layer.route) {
    var match = layer.regexp.toString().match(/\/\^\\(\/\w+)/)
    if (!match) return
    var path = match[1]

    var stack = layer.handle.stack
    var nestedRoutes = stack.map((l) => {
      var rs = recurse(l)
        .map((route) => {
          route.path = path.concat(route.path)
          return route
        })

      return rs
    })
    var rs = [].concat.apply([], nestedRoutes)
    return rs
  } else {
    var path = layer.route.path
    if (!path) return
    var rs = layer.route.stack.reduce((acc, l) => {
      acc.push({ path, method: l.method })
      return acc
    }, [])
    return rs
  }
}

module.exports = SystemController
