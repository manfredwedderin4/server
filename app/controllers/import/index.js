const fs = require('fs')
const tar = require('tar-stream')
const pump = require('pump')
const debug = require('debug')('@coboxcoop/server:import')
const zlib = require('zlib')
const concat = require('concat-stream')
const { values } = Object

class ImportController {
  constructor (app) {
    this.app = app
  }

  // POST /api/import
  async create (params, opts = {}) {

    return new Promise((resolve, reject) => {

      var filepath = params.file.path
      if (!filepath) return false // error, no file found

      // note: zlib is only meant for small (< 128KB data)
      var files = {}
      var extract = tar.extract()
      extract.on('entry', function (header, stream, next) {
        stream.pipe(concat((contents) => {
          files[header.name] = contents.toString()
          return next()
        }))
      })
      extract.on('finish', async () => {
        if (files.VERSION !== '1') return reject(new Error('Version mismatch.'))
        var data = JSON.parse(files['backup.cobox.json'])

        var { id } = this.app.sessions.create({ parentKey: data.parentKey })

        this.app.sessions.change(id, async (err, session) => {
          if (err) return reject(err)

          var {
            spaces: { store: spaces },
            replicators: { store: replicators },
            seeders: { store: seeders },
            profile
          } = session.api

          await profile.update(data.profile)
          await createItems(spaces, data.spaces)
          await createItems(replicators, data.replicators)
          await createItems(seeders, data.seeders)
          await spaces.load()
          await replicators.load()

          resolve(session.id)

          async function createItems (repository, collection) {
            values(collection).map(async (params) => {
              const entry = await repository.create(params)
              var identity = {
                name: profile.name,
                publicKey: profile.publicKey
              }
              await entry.constructor.onCreate(entry, { identity })
              return entry
            })
          }
        })
      })

      pump(fs.createReadStream(filepath), zlib.createGunzip(), extract, (err) => {
        if (err) return reject(err)
      })
    })
  }
}

module.exports = ImportController
