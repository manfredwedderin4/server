const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const ProfileDecorator = require('../../../lib/decorators/profile')

module.exports = class SessionController {
  constructor (app) {
    this.app = app
  }

  // GET /api/session
  async list (params, opts = {}) {
    var sesh = this.app.sessions.list()
    return sesh.map((s) => ProfileDecorator(s).toJSON())
  }

  // POST /api/session
  async create (params, opts = {}) {
    var { id } = this.app.sessions.create(opts)
    return new Promise((resolve, reject) => {
      this.change(id, function (err, session) {
        if (err) return reject(err)
        resolve({
          sessionId: id
        })
      })
    })
  }

  // GET /api/session/:id ?
  async change (params, opts = {}) {
    return new Promise((resolve, reject) => {
      this.app.sessions.change(params.id, err => {
        if (err) reject(err)
        else resolve({ success: true })
      })
    })
  }
}
