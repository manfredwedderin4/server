const assert = require('assert')
const resourceParams = require('../../helpers/resource-params')
const SpaceDecorator = require('../../../lib/decorators/space')
const ConnectionsController = require('./connections')
const { assign } = Object

class ReplicatorsController {
  constructor (api) {
    this.api = api
    this.repository = api.replicators.store
    this.connections = new ConnectionsController(api, this)
  }

  // GET /api/replicators
  async index (params, opts = {}) {
    let replicators = await this.repository.all()

    if (!opts.decorate) return replicators

    return replicators
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  // GET /api/replicators/:id
  async show (params, opts = {}) {
    let replicator = await this.repository.findBy(resourceParams(params))
    assert(replicator, 'does not exist')

    if (!opts.decorate) return replicator

    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  // POST /api/replicators
  async create (params, opts = {}) {
    let replicator = await this.repository.create(resourceParams(params))
    assert(replicator, 'failed to create')

    if (!opts.decorate) return replicators

    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  // DELETE /api/replicators/:id
  async destroy (params, opts = {}) {
    let replicator = await this.repository.findBy(resourceParams(params))
    assert(replicator, 'does not exist')

    try {
      await replicator.destroy()
      await this.repository.remove(resourceParams(params))
    } catch (err) {
      throw err
    }

    if (!opts.decorate) return replicator
    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }
}

module.exports = ReplicatorsController
