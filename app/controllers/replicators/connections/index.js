const debug = require('@coboxcoop/logger')('@coboxcoop/server:replicator/connections')
const through = require('through2')
const assert = require('assert')
const collect = require('collect-stream')

const SpaceDecorator = require('../../../../lib/decorators/space')
const resourceParams = require('../../../helpers/resource-params')

const { hex } = require('../../../../util')

class ReplicatorConnectionsController {
  constructor (api, replicators) {
    this.api = api
    this.replicators = replicators
  }

  // GET /api/replicators/:id/connections
  async index (params, opts = {}) {
    let replicator = await this.replicators.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      var stream = replicator.db.connections.createReadStream()
        .pipe(format('REPLICATOR', replicator))

      collect(stream, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // POST /api/replicators/:id/connections
  async create (params, opts = {}) {
    let replicator = await this.replicators.show(resourceParams(opts))

    replicator.swarm()

    await replicator.db.connections.batch([{
      type: 'put',
      key: hex(this.api.profile.publicKey),
      value: {
        type: 'peer/connection',
        version: '1.0.0',
        timestamp: Date.now(),
        content: { connected: true }
      }
    }])

    if (!opts.decorate) return replicator
    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  // POST /api/replicators/connections
  async createAll (params, opts = {}) {
    let rs = await this.replicators.index(resourceParams(opts))

    await Promise.all(rs.map((replicator) => {
      return new Promise((resolve, reject) => {
        replicator.swarm()
        replicator.db.connections.batch([{
          type: 'put',
          key: hex(this.api.profile.publicKey),
          value: {
            type: 'peer/connection',
            version: '1.0.0',
            timestamp: Date.now(),
            content: { connected: true }
          }
        }]).catch((err) => {
          if (err.code === 'ERR_ASSERTION') return resolve()
          reject(err)
        }).then(resolve)
      })
    }))

    if (!opts.decorate) return rs
    let decorators = rs.map(SpaceDecorator)
    return decorators.map((d) => d.toJSON())
  }

  // DELETE /api/replicators/:id/connections
  async destroy (params, opts = {}) {
    let replicator = await this.replicators.show(resourceParams(params))

    replicator.unswarm()

    await replicator.db.connections.batch([{
      type: 'put',
      key: hex(this.api.profile.publicKey),
      value: {
        type: 'peer/connection',
        version: '1.0.0',
        timestamp: Date.now(),
        content: { connected: false }
      }
    }])

    if (!opts.decorate) return replicator
    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  // DELETE /api/replicators/connections
  async destroyAll (params, opts = {}) {
    let rs = await this.replicators.index(resourceParams(opts))

    await Promise.all(rs.map((replicator) => {
      return new Promise((resolve, reject) => {
        replicator.unswarm()
        replicator.db.connections.batch([{
          type: 'put',
          key: hex(this.api.profile.publicKey),
          value: {
            type: 'peer/connection',
            version: '1.0.0',
            timestamp: Date.now(),
            content: { connected: false }
          }
        }]).catch((err) => {
          if (err.code === 'ERR_ASSERTION') return resolve()
          reject(err)
        }).then(resolve)
      })
    }))

    if (!opts.decorate) return rs
    let decorators = rs.map(SpaceDecorator)
    return decorators.map((d) => d.toJSON())
  }
}

function format (resourceType, entry) {
  return through.obj(function (msg, _, next) {
    var value = {
      resourceType,
      feedId: msg.key,
      address: hex(entry.address),
      data: msg.value
    }
    this.push(value)
    next()
  })
}

module.exports = ReplicatorConnectionsController
