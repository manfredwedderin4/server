const debug = require('@coboxcoop/logger')('@coboxcoop/server:events')

class EventsController {
  constructor (api) {
    this.api = api
  }

  // WS /api
  live (params, opts = {}) {
    return this.api.events.createReadStream()
  }
}

module.exports = EventsController
