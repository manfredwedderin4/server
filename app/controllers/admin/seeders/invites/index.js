const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const Invite = require('@coboxcoop/key-exchange')
const { loadKey } = require('@coboxcoop/keys')
const { encodings } = require('@coboxcoop/schemas')
const pick = require('lodash.pick')
const collect = require('collect-stream')

const SpaceDecorator = require('../../../../../lib/decorators/space')
const resourceParams = require('../../../../helpers/resource-params')
const ConnectionsController = require('../connections')
const { hex } = require('../../../../../util')

const PeerInvite = encodings.peer.invite
const PeerAbout = encodings.peer.about
const { assign } = Object

class SeederInvitesController {
  constructor (api, seeders) {
    this.api = api
    this.seeders = seeders
  }

  // GET /api/admin/seeders/invites/accept
  async accept (params, opts = {}) {
    let seederParams

    try {
      seederParams = Invite.open(Buffer.from(params.code, 'hex'), this.api.profile.identity)
    } catch (error) {
      throw new Error('invite code invalid')
    }

    let seeder = await this.seeders.create(seederParams)
    var decorator = SpaceDecorator(seeder)
    var _opts = assign({}, opts, resourceParams(decorator.toJSON()))
    return await this.seeders.connections.create(null, _opts)
  }

  // GET /api/admin/seeders/:id/invites
  async index (params, opts = {}) {
    let seeder = await this.seeders.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      const query = { $filter: { value: { type: 'peer/invite', timestamp: { $gt: 0 } } } }
      const invites = seeder.log.read({ query: [query] })

      collect(invites, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // POST /api/admin/seeders/:id/invites
  async create (params, opts = {}) {
    let seeder = await this.seeders.show(resourceParams(opts))
    let code = Invite.create(inviteParams(seeder, params))

    let msg = {
      type: 'peer/invite',
      version: '1.0.0',
      timestamp: Date.now(),
      author: hex(this.api.profile.publicKey),
      content: {
        publicKey: hex(params.publicKey)
      }
    }

    await seeder.log.publish(msg, { valueEncoding: PeerInvite })

    msg.content.code = hex(code)

    return msg
  }
}

function inviteParams (seeder, params) {
  var encryptionKey = loadKey(seeder.storage, 'encryption_key')
  var seederParams = assign(SpaceDecorator(seeder).toJSON(), params)
  var picked = pick(seederParams, ['publicKey', 'address', 'name'])
  return assign({ encryptionKey }, picked)
}

module.exports = SeederInvitesController
