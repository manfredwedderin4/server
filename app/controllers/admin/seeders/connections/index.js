const debug = require('@coboxcoop/logger')('@coboxcoop/server:admin/seeder/connections')
const assert = require('assert')
const collect = require('collect-stream')
const through = require('through2')

const SpaceDecorator = require('../../../../../lib/decorators/space')
const resourceParams = require('../../../../helpers/resource-params')
const { hex } = require('../../../../../util')

class SeederConnectionsController {
  constructor (api, seeders) {
    this.api = api
    this.seeders = seeders
  }

  // GET /api/admin/seeders/:id/connections
  async index (params, opts = {}) {
    let seeder = await this.seeders.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      var connections = seeder.db.connections.createReadStream()
        .pipe(format('ADMIN_SEEDER', seeder))

      collect(connections, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // POST /api/admin/seeders/:id/connections
  async create (params, opts = {}) {
    let seeder = await this.seeders.show(resourceParams(opts))

    seeder.swarm()

    await seeder.db.connections.batch([{
      type: 'put',
      key: hex(this.api.profile.publicKey),
      value: {
        type: 'peer/connection',
        version: '1.0.0',
        timestamp: Date.now(),
        content: { connected: true }
      }
    }])

    if (!opts.decorate) return seeder
    let decorator = SpaceDecorator(seeder)
    return decorator.toJSON()
  }

  // POST /api/admin/seeders/connections
  async createAll (params, opts = {}) {
    let ds = await this.seeders.index(resourceParams(opts))

    await Promise.all(ds.map((seeder) => {
      return new Promise((resolve, reject) => {
        seeder.swarm()
        seeder.db.connections.batch([{
          type: 'put',
          key: hex(this.api.profile.publicKey),
          value: {
            type: 'peer/connection',
            version: '1.0.0',
            timestamp: Date.now(),
            content: { connected: true }
          }
        }]).catch((err) => {
          if (err.code === 'ERR_ASSERTION') return resolve()
          reject(err)
        }).then(resolve)
      })
    }))

    if (!opts.decorate) return ds

    return ds
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  // DELETE /api/admin/seeders/:id/connections
  async destroy (params, opts = {}) {
    let seeder = await this.seeders.show(resourceParams(params))

    seeder.unswarm()

    await seeder.db.connections.batch([{
      type: 'put',
      key: hex(this.api.profile.publicKey),
      value: {
        type: 'peer/connection',
        version: '1.0.0',
        timestamp: Date.now(),
        content: { connected: false }
      }
    }])

    if (!opts.decorate) return seeder
    let decorator = SpaceDecorator(seeder)
    return decorator.toJSON()
  }

  // DELETE /api/admin/seeders/connections
  async destroyAll (params, opts = {}) {
    let ds = await this.seeders.index(resourceParams(opts))

    await Promise.all(ds.map((seeder) => {
      return new Promise((resolve, reject) => {
        seeder.unswarm()
        seeder.db.connections.batch([{
          type: 'put',
          key: hex(this.api.profile.publicKey),
          value: {
            type: 'peer/connection',
            version: '1.0.0',
            timestamp: Date.now(),
            content: { connected: false }
          }
        }]).catch((err) => {
          if (err.code === 'ERR_ASSERTION') return resolve()
          reject(err)
        }).then(resolve)
      })
    }))

    if (!opts.decorate) return ds

    return ds
      .map(SpaceDecorator)
      .map((d) => d.toJSON())
  }
}

function format (resourceType, entry) {
  return through.obj(function (msg, _, next) {
    var value = {
      resourceType,
      peerId: msg.key,
      address: hex(entry.address),
      data: msg.value
    }
    this.push(value)
    next()
  })
}

module.exports = SeederConnectionsController
