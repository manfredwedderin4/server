const debug = require('@coboxcoop/logger')('@coboxcoop/server:admin/seeder/peers')
const assert = require('assert')
const collect = require('collect-stream')
const through = require('through2')

const SpaceDecorator = require('../../../../../lib/decorators/space')
const resourceParams = require('../../../../helpers/resource-params')
const { hex } = require('../../../../../util')

const ABOUT_PEER = 'peer/about'

class SeederPeersController {
  constructor (api, seeders) {
    this.api = api
    this.seeders = seeders
  }

  // GET /api/admin/seeders/:id/peers
  async index (params, opts = {}) {
    let seeder = await this.seeders.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      let query = { $filter: { value: { type: ABOUT_PEER, timestamp: { $gt: 0 }} } }
      var stream = seeder.log.read({ query: [query] })
        .pipe(format('ADMIN_SEEDER', seeder))

      collect(stream, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }
}

function format (resourceType, entry) {
  return through.obj(function (msg, _, next) {
    this.push({
      resourceType,
      feedId: msg.key,
      address: hex(entry.address),
      data: msg.value
    })
    next()
  })
}

module.exports = SeederPeersController
