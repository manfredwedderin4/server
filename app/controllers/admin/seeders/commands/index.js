const BroadcastCommandsController = require('./broadcasts')
const ReplicateCommandsController = require('./replicates')

class SeederCommandsController {
  constructor (api, seeders) {
    this.broadcasts = new BroadcastCommandsController(api, seeders)
    this.replicates = new ReplicateCommandsController(api, seeders)
  }
}

module.exports = SeederCommandsController
