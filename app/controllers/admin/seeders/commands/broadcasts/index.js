const debug = require('@coboxcoop/logger')('@coboxcoop/server:broadcasts')
const assert = require('assert')
const pick = require('lodash.pick')
const { encodings, validators } = require('@coboxcoop/schemas')
const collect =  require('collect-stream')

const Announce = encodings.command.announce
const Hide = encodings.command.hide
const isAnnounce = validators.command.announce
const isHide = validators.command.hide

const resourceParams = require('../../../../../helpers/resource-params')
const { hex } = require('../../../../../../util')

const ANNOUNCE = 'command/announce'
const HIDE = 'command/hide'

class BroadcastCommandsController {
  constructor (api, seeders) {
    this.api = api
    this.seeders = seeders
  }

  // GET /api/admin/seeders/:id/commands/broadcasts
  async index (params, opts = {}) {
    let seeder = await this.seeders.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      const query = { $filter: { value: { type: { $in: [ANNOUNCE, HIDE] }, timestamp: { $gt: 0 } } } }
      const commands = seeder.log.read({ query: [query] })

      collect(commands, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // POST /api/admin/seeders/:id/commands/announce
  async create (params, opts) {
    let seeder = await this.seeders.show(resourceParams(params))

    const msg = {
      type: ANNOUNCE,
      version: '1.0.0',
      timestamp: Date.now(),
      author: hex(this.api.profile.publicKey)
    }

    assert(isAnnounce(msg), 'invalid parameters')

    await seeder.log.publish(msg, { valueEncoding: Announce })

    return msg
  }

  // POST /api/admin/seeders/:id/commands/hide
  async destroy (params, opts) {
    let seeder = await this.seeders.show(resourceParams(params))

    const msg = {
      type: HIDE,
      version: '1.0.0',
      timestamp: Date.now(),
      author: hex(this.api.profile.publicKey)
    }

    assert(isHide(msg), 'invalid parameters')

    await seeder.log.publish(msg, { valueEncoding: Hide })

    return msg
  }

  // PATCH /api/admin/seeders/:id/commands/broadcasts
  async update (params, opts) {
    let commands = await this.index(params, opts)
    let command = commands[commands.length - 1]
    if (command === ANNOUNCE) return await this.create(params, opts)
    else if (command === HIDE) return await this.destroy(params, opts)
    else throw new Error('invalid command')
  }
}

module.exports = BroadcastCommandsController
