const debug = require('@coboxcoop/logger')('@coboxcoop/server:broadcasts')
const assert = require('assert')
const pick = require('lodash.pick')
const { encodings, validators } = require('@coboxcoop/schemas')
const collect = require('collect-stream')

const Replicate = encodings.command.replicate
const Unreplicate = encodings.command.unreplicate
const isReplicate = validators.command.replicate
const isUnreplicate = validators.command.unreplicate

const resourceParams = require('../../../../../helpers/resource-params')
const { hex } = require('../../../../../../util')

const REPLICATE = 'command/replicate'
const UNREPLICATE = 'command/unreplicate'

class ReplicateCommandsController {
  constructor (api, seeders) {
    this.api = api
    this.seeders = seeders
  }

  // GET /api/admin/seeders/:id/commands/replicates
  async index (params, opts = {}) {
    let seeder = await this.seeders.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      const query = { $filter: { value: { type: { $in: [REPLICATE, UNREPLICATE] }, timestamp: { $gt: 0 } } } }
      const commands = seeder.log.read({ query: [query] })

      collect(commands, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // POST /api/admin/seeders/:id/commands/replicate
  async create (params, opts = {}) {
    let seeder = await this.seeders.show(resourceParams(opts))

    let msg = {
      type: REPLICATE,
      version: '1.0.0',
      timestamp: Date.now(),
      author: hex(this.api.profile.publicKey),
      content: resourceParams(params)
    }

    assert(isReplicate(msg), 'invalid parameters')

    await seeder.log.publish(msg, { valueEncoding: Replicate })

    return msg
  }

  // POST /api/admin/seeders/:id/commands/unreplicate
  async destroy (params, opts = {}) {
    let seeder = await this.seeders.show(resourceParams(opts))

    let msg = {
      type: UNREPLICATE,
      version: '1.0.0',
      timestamp: Date.now(),
      author: hex(this.api.profile.publicKey),
      content: resourceParams(params)
    }

    assert(isUnreplicate(msg), 'invalid parameters')

    await seeder.log.publish(msg, { valueEncoding: Unreplicate })

    return msg
  }
}

module.exports = ReplicateCommandsController
