const debug = require('@coboxcoop/logger')('@coboxcoop/server:admin-seeders')
const assert = require('assert')
const { Superusers } = require('@coboxcoop/superusers')
const pick = require('lodash.pick')
const SpaceDecorator = require('../../../../lib/decorators/space')
const { removeEmpty } =  require('../../../../util')
const { assign } = Object
const { isArray } = Array
const resourceParams = require('../../../helpers/resource-params')

const SeederConnectionsController = require('./connections')
const SeederInvitesController = require('./invites')
const SeederCommandsController = require('./commands')
const SeederPeersController = require('./peers')

const LocalSeedersController = require('../../seeders')

const { hex } = require('../../../../util')

class SeedersController {
  constructor (api) {
    this.api = api
    this.repository = this.api.admin.seeders.store
    this.seeders = new LocalSeedersController(api)
    this.connections = new SeederConnectionsController(api, this)
    this.invites = new SeederInvitesController(api, this)
    this.commands = new SeederCommandsController(api, this)
    this.peers = new SeederPeersController(api, this)
  }

  // GET /api/admin/seeders
  async index (params, opts = {}) {
    let seeders = await this.repository.all()
    if (!opts.decorate) return seeders

    let response = seeders
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())

    return response
  }

  // GET /api/admin/seeders/:id
  async show (params, opts = {}) {
    let seeder = await this.repository.findBy(seederParams(params))
    if (!opts.decorate) return seeder
    let decorator = SpaceDecorator(seeder)
    return decorator.toJSON()
  }

  // POST /api/admin/seeders
  async create (params, opts = {}) {
    if (params.code) return await this.invites.accept(params, opts)
    params = await this._handlePublicKey(params)
    let seeder = await this.repository.create(seederParams(params))
    Superusers.onCreate(seeder, { identity: this.api.profile})
    let decorator = SpaceDecorator(seeder)
    let commands = await this._handleCommands(params.commands, decorator.toJSON())

    if (!opts.decorate) return seeder
    return removeEmpty(assign(decorator.toJSON(), { commands }))
  }

  // DELETE /api/admin/seeders/:id
  async destroy (params, opts = {}) {
    let seeder = await this.repository.findBy(resourceParams(params))
    assert(seeder, 'does not exist')

    try {
      await seeder.destroy()
      await this.repository.remove(resourceParams(params))
    } catch (err) {
      throw err
    }

    if (!opts.decorate) return seeder
    let decorator = SpaceDecorator(seeder)
    return decorator.toJSON()
  }

  // --------------------- Helpers ---------------------- //

  async _handleCommands (cmds, opts) {
    if (!isArray(cmds)) return

    const mappings = {
      announce: (params, opts) => this.commands.broadcasts.create(opts),
      hide: (params, opts) => this.commands.broadcasts.destroy(opts),
      replicate: (params, opts) => this.commands.replicates.create(params, opts),
      unreplicate: (params, opts) => this.commands.replicates.destroy(params, opts)
    }

    let promises = cmds
      .filter((command) => mappings[command.action])
      .map((command) => mappings[command.action](command, opts))

    return await Promise.all(promises)
  }

  async _handlePublicKey (params) {
    if (!params.publicKey) return params
    let seeder = await this.seeders.show(params, { decorate: true })
    return assign({}, params, seeder)
  }
}

function seederParams (params) {
  return pick(params, ['name', 'address', 'encryptionKey'])
}

module.exports = SeedersController
