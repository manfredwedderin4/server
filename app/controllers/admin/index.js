const SeedersController = require('./seeders')

class AdminController {
  constructor (api) {
    this.seeders = new SeedersController(api)
  }
}

module.exports = AdminController
