const debug = require('debug')('@coboxcoop/server:export')
const sodium = require('sodium-native')
const { createPdf } = require('paper-key-backup')
const { loadKey } = require('@coboxcoop/keys')
const tar = require('tar-stream')
const zlib = require('zlib')
const pumpify = require('pumpify')

const SpaceDecorator = require('../../../lib/decorators/space')
const { hex } = require('../../../util')
const PARENT_KEY = 'parent_key'
const VERSION = require('../../../version')

class ExportController {
  constructor (api) {
    this.api = api
    this.spaces = this.api.spaces.store
    this.seeders = this.api.admin.seeders.store
    this.replicators = this.api.replicators.store
  }

  // GET /api/export
  async index (params, opts = {}) {
    const data = {}
    const pack = tar.pack()

    // TODO: leaky.
    // instead of settings.sessionStorage, use current app.session.storage
    const parentKey = loadKey(this.api.settings.sessionStorage, PARENT_KEY)
    data.parentKey = hex(parentKey)
    sodium.sodium_memzero(parentKey)

    data.settings = this.api.settings
    data.profile = this.api.profile.identity

    const spaces = await this.spaces.all()
    data.spaces = spaces.reduce(forExport, {})
    const replicators = await this.replicators.all()
    data.replicators = replicators.reduce(forExport, {})
    const seeders = await this.seeders.all()
    data.seeders = seeders.reduce(forExport, {})

    pack.entry({ name: 'VERSION' }, VERSION, (err) => {
      if (err) throw err
      pack.entry({ name: 'backup.cobox.json' }, JSON.stringify(data, null, 2), (err) => {
        if (err) throw err
        pack.finalize()
      })
    })

    return pumpify(pack, zlib.createGzip())

    function forExport (acc, resource) {
      acc[hex(resource.address)] = SpaceDecorator(resource).toJSON()
      return acc
    }
  }
}

module.exports = ExportController
