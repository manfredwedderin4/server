const debug = require('@coboxcoop/logger')('@coboxcoop/server:seeders')
const through = require('through2')
const pick = require('lodash.pick')
const collect = require('collect-stream')

const SEEDER_CONNECTED = 'SEEDER_CONNECTED'
const SEEDER_DISCONNECTED = 'SEEDER_DISCONNECTED'

class LocalSeedersController {
  constructor (api) {
    this.api = api
    this.repository = api.seeders.store
  }

  // WS /api/seeders/.websocket
  live (params, opts = {}) {
    return this.repository.createLiveStream()
      .pipe(format())
  }

  // GET /api/seeders
  async index (params, opts = {}) {
    return await new Promise((resolve, reject) => {
      const seeders = this.repository.createReadStream()
        .pipe(format())

      collect(seeders, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // GET /api/seeders/:id
  async show (params, opts = {}) {
    const seeder = await this._findSeeder(params)
    if (!opts.decorate) return seeder
    return seeder.content
  }

  // --------------------- Helpers ---------------------- //

  _findSeeder (params) {
    return new Promise((resolve, reject) => {
      this.repository.get(params.publicKey, (err, msg) => {
        if (err) return reject(err)
        if (err && err.notFound) return reject(new Error('seeder not seen on this network'))
        else resolve(msg)
      })
    })
  }
}

function format () {
  return through.obj(function (msg, _, next) {
    if (msg.sync) return next()

    var isBroadcasting = msg.value.content.broadcasting
    var address = msg.value.content.address

    this.push({
      type: isBroadcasting ? SEEDER_CONNECTED : SEEDER_DISCONNECTED,
      data: { address, ...pick(msg.value, ['author', 'timestamp']) }
    })

    next()
  })
}

module.exports = LocalSeedersController
