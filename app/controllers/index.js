const AdminController = require('./admin')
const SeedersController = require('./seeders')
const EventsController = require('./events')
const SpacesController = require('./spaces')
const ReplicatorsController = require('./replicators')
const ProfileController = require('./profile')
const SystemController = require('./system')
const ExportController = require('./export')
const ImportController = require('./import')
const SessionController = require('./session')

class ApplicationController {
  constructor (app) {
    const session = app.get('session')
    const api = session.api

    this.admin = new AdminController(api)
    this.seeders = new SeedersController(api)
    this.events = new EventsController(api)
    this.profile = new ProfileController(api)
    this.replicators = new ReplicatorsController(api)
    this.spaces = new SpacesController(api)
    this.system = new SystemController(api)
    this.export = new ExportController(api)
    this.import = new ImportController(app)
    this.session = new SessionController(app)
  }
}

module.exports = ApplicationController
