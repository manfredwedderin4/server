const debug = require('@coboxcoop/logger')('@coboxcoop/server:spaces')
const assert = require('assert')
const crypto = require('@coboxcoop/crypto')
const { Space } = require('@coboxcoop/space')
const { encodings } = require('@coboxcoop/schemas')
const { assign } = Object

const ConnectionsController = require('./connections')
const MountsController = require('./mounts')
const DriveController = require('./drive')
const InvitesController = require('./invites')
const PeersController = require('./peers')

const SpaceDecorator = require('../../../lib/decorators/space')
const resourceParams = require('../../helpers/resource-params')

const { hex } = require('../../../util')

const PeerAbout = encodings.peer.about
const SpaceAbout = encodings.space.about

class SpacesController {
  constructor (api) {
    this.api = api
    this.repository = api.spaces.store
    this.connections = new ConnectionsController(api, this)
    this.mounts = new MountsController(api, this)
    this.drive = new DriveController(api, this)
    this.invites = new InvitesController(api, this)
    this.peers = new PeersController(api, this)
  }

  // GET /api/spaces
  async index (params, opts = {}) {
    let spaces = await this.repository.all()

    if (!opts.decorate) return spaces

    return spaces
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  // GET /api/spaces/:id
  async show (params, opts = {}) {
    let space = await this.repository.findBy(resourceParams(params))
    assert(space, 'does not exist')

    if (!opts.decorate) return space

    let decorator = SpaceDecorator(space)
    return decorator.toJSON()
  }

  // POST /api/spaces
  async create (params, opts = {}) {
    if (params.code) return await this.invites.accept(params, opts)

    const space = await this.repository.create(createParams(params))
    assert(space, 'failed to create')
    await Space.onCreate(space, {
      identity: {
        name: this.api.profile.name,
        publicKey: this.api.profile.publicKey
      }
    })

    if (!opts.decorate) return space
    const decorator = SpaceDecorator(space)
    return decorator.toJSON()
  }

  // DELETE /api/spaces/:id
  async destroy (params, opts = {}) {
    let space = await this.repository.findBy(resourceParams(params))
    assert(space, 'does not exist')

    try {
      await space.destroy()
      // only remove from repo if space was successfully destroyed
      await this.repository.remove(resourceParams(params))
    } catch (err) {
      throw err
    }

    if (!opts.decorate) return seeder
    let decorator = SpaceDecorator(space)
    return decorator.toJSON()
  }
}

function createParams (params) {
  var resParams = resourceParams(params)
  if (!params.address && !params.encryptionKey) {
    return assign({}, resParams, {
      address: hex(crypto.address()),
      encryptionKey: hex(crypto.encryptionKey())
    })
  }
  return resParams
}

module.exports = SpacesController
