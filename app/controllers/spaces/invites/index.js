const debug = require('@coboxcoop/logger')('@coboxcoop/server:space/invites')
const Invite = require('@coboxcoop/key-exchange')
const { loadKey } = require('@coboxcoop/keys')
const { encodings } = require('@coboxcoop/schemas')
const pick = require('lodash.pick')
const collect = require('collect-stream')

const SpaceDecorator = require('../../../../lib/decorators/space')
const resourceParams = require('../../../helpers/resource-params')
const { hex } = require('../../../../util')

const { assign } = Object
const PeerInvite = encodings.peer.invite
const PeerAbout = encodings.peer.about

class SpaceInvitesController {
  constructor (api, spaces) {
    this.api = api
    this.spaces = spaces
  }

  // GET /api/spaces/invites/accept?code=
  async accept (params, opts = {}) {
    let spaceParams

    try {
      spaceParams = Invite.open(Buffer.from(params.code, 'hex'), this.api.profile.identity)
    } catch (error) {
      throw new Error('invite code invalid')
    }

    let space = await this.spaces.create(spaceParams)
    var decorator = SpaceDecorator(space)
    var _opts = assign({}, opts, resourceParams(decorator.toJSON()))
    await this.spaces.connections.create(null, _opts)
    return await this.spaces.mounts.create({ location: this.api.settings.mount }, _opts)
  }

  // GET /api/spaces/:id/invites
  async index (params, opts = {}) {
    let space = await this.spaces.show(resourceParams(params))

    return await new Promise((resolve, reject) => {
      const query = { $filter: { value: { type: 'peer/invite', timestamp: { $gt: 0 } } } }
      const invites = space.log.read({ query: [query] })

      collect(invites, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  // POST /api/spaces/:id/invites
  async create (params, opts = {}) {
    let space = await this.spaces.show(resourceParams(opts))
    let code = Invite.create(inviteParams(space, params))

    let msg = {
      type: 'peer/invite',
      version: '1.0.0',
      timestamp: Date.now(),
      author: hex(this.api.profile.publicKey),
      content: { publicKey: hex(params.publicKey) }
    }

    await space.log.publish(msg, { valueEncoding: PeerInvite })

    msg.content.code = hex(code)

    return msg
  }
}

function inviteParams (space, params) {
  var encryptionKey = loadKey(space.storage, 'encryption_key')
  var spaceParams = assign(SpaceDecorator(space).toJSON(), params)
  var picked = pick(spaceParams, ['publicKey', 'address', 'name'])
  return assign({ encryptionKey }, picked)
}

module.exports = SpaceInvitesController
