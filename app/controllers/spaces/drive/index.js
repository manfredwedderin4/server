const debug = require('@coboxcoop/logger')('@coboxcoop/server:space/drive')
const open = require('open')
const assert = require('assert')
const path = require('path')
const resourceParams = require('../../../helpers/resource-params')

class DriveController {
  constructor (api) {
    this.api = api
    this.repository = api.spaces.store
  }

  // GET /api/spaces/:id/drive
  async open (params, opts = {}) {
    let space = await this.repository.findBy(resourceParams(params))
    assert(space, 'does not exist')

    try {
      console.log()
      await open(path.join(this.api.settings.mount, space.name))
    } catch (err) {
      console.error(err)
      throw new Error('space not mounted')
    }

    return true
  }

  // GET /api/spaces/:id/drive/history
  async history (params, opts = {}) {
    let space = await this.repository.findBy(resourceParams(opts))
    assert(space, 'does not exist')

    await space.drive.ready()

    return await space.state.query(params.query)
  }

  // GET /api/spaces/:id/drive/readdir
  async readdir (params, opts = {}) {
    let space = await this.repository.findBy(resourceParams(params))
    assert(space, 'does not exist')

    return await space.drive.ls(params.dir)
  }

  // GET /api/spaces/:id/drive/stat
  async stat (params, opts = {}) {
    let space = await this.repository.findBy(resourceParams(params))
    assert(space, 'does not exist')

    await space.drive.ready()

    return { size: await space.drive.size() }
  }
}

module.exports = DriveController
