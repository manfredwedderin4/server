const debug = require('@coboxcoop/logger')('@coboxcoop/server:space/mounts')
const assert = require('assert')
const spaceParams = require('../../../helpers/resource-params')
const SpaceDecorator = require('../../../../lib/decorators/space')
const { assign } = Object
const path = require('path')

class MountsController {
  constructor (api) {
    this.api = api
    this.repository = api.spaces.store
  }

  // GET /api/spaces/mounts
  async index (params, opts = {}) {
    let spaces = await this.repository.all()
    let mounts = spaces.filter((space) => space.drive.location)

    if (!opts.decorate) return mounts

    return mounts
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  // POST /api/spaces/:id/mounts
  async create (params, opts = {}) {
    let space = await this.repository.findBy(spaceParams(opts))
    assert(space, 'does not exist')

    let location = path.join(params.location, space.name)
    await space.drive.mount(assign(params, { location }))

    let decorator = SpaceDecorator(space)
    return assign(decorator.toJSON(), { location })
  }

  // POST /api/spaces/mounts
  async createAll (params, opts = {}) {
    let spaces = await this.repository.all()

    var mounts = spaces.filter((space) => !space.drive._unmount)

    mounts.forEach((space) => space.drive.mount({
      location: path.join(params.location, space.name)
    }))

    if (!opts.decorate) return mounts

    return mounts
      .map(SpaceDecorator)
      .map((d) => d.toJSON())
  }

  // DELETE /api/spaces/:id/mounts
  async destroy (params = {}, opts) {
    let space = await this.repository.findBy(spaceParams(params))
    assert(space, 'does not exist')

    let location = await space.drive.unmount()

    let decorator = SpaceDecorator(space)
    return assign(decorator.toJSON(), { location })
  }

  // DELETE /api/spaces/mounts
  async destroyAll (params, opts) {
    let spaces = await this.repository.all()

    var mounts = spaces.filter((space) => space.drive._unmount)
    mounts.forEach((space) => space.drive.unmount())

    if (!opts.decorate) return mounts

    return mounts
      .map(SpaceDecorator)
      .map((d) => d.toJSON())
  }
}

module.exports = MountsController
