module.exports = function now () {
  return new Date().toISOString().replace(/T/, '-').replace(/\..+/, '')
}
