const Space = require('@coboxcoop/space')
const pick = require('lodash.pick')
const { assign } = Object

module.exports = function (storage, opts = {}) {
  return function (params) {
    return Space(
      storage,
      params.address,
      opts.identity,
      assign({}, params, opts)
    )
  }
}
