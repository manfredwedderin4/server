const Replicator = require('@coboxcoop/replicator')
const pick = require('lodash.pick')
const { assign } = Object

module.exports = function (storage, opts) {
  return function (params) {
    return Replicator(
      storage,
      params.address,
      opts.identity,
      assign({}, params, opts)
    )
  }
}
