const Superusers = require('@coboxcoop/superusers')
const { assign } = Object

module.exports = function (storage, opts = {}) {
  return function (params) {
    return Superusers(
      storage,
      params.address,
      opts.identity,
      assign({}, params, opts)
    )
  }
}
