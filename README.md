# Server

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

## Table of Contents

* [About](#about)
* [Installing and Updating](#installing-and-updating)
  * [Install & Update Script](#install-and-update-script)
  * [Troubleshooting on Linux](#troubleshooting-on-linux)
  * [Verify Installation](#verify-installation)
  * [NPM Install](#npm-install)
  * [Git Install](#git-install)
* [Usage](#usage)
* [API](#api)
* [Contributing](#contributing)
* [License](#license)

## About

**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`server` is a core component of the CoBox App. It exposes a VueJS UI, an ExpressJS JSON API, and a Yargs CLI for interacting with CoBox 'folders', 'seeds' and 'seeders'. Check out our [docs](https://docs.cobox.cloud) for more details.

## Installing and Updating

### Install and Update Script

To install or update @coboxcoop/server, you should run the [install script](./download.sh). To do that, you may either download and run the script manually, or use the following cURL or Wget command:

```
curl -o- https://cobox.cloud/releases/cobox-v1.0.0-alpha.1/download.sh | bash
```

```
wget -qO- https://cobox.cloud/releases/cobox-1.0.0-alpha.1/download.sh | bash
```

Running either of the above commands downloads a script and runs it. The script downloads a tarball that contains the release binary required by your operating system with additional assets and unpacks the contents to the install directory.

### Troubleshooting on Linux

On Linux, after running the install script, if you get `cobox: command not found` or see no feedback from your terminal after you type `command -v cobox`, simply close your current terminal, open a new terminal, and try verifying again.

### Verify Installation

To verify that CoBox has been installed correctly, do:

```
command -v cobox
```

### NPM Install

```
npm install -g @coboxcoop/server

cobox
```

### Git Install

Before you get started, ensure you have `git` and `node` installed. We currently release using `v12.16.3` which we recommend using. You can download and install nodejs from your distribution package manager, or [direct from their website](https://nodejs.org/en/download/).

```
# clone the repository
git clone http://gitlab.com/coboxcoop/server && cd server

# install dependencies
npm install

# start the server
node bin start

# use the cli
node bin
```

## Usage

CoBox Server is accessible through a UI in the browser, and through a CLI in the terminal.

```
CoBox 1.0 - an encrypted peer-to-peer file system and distributed back-up tool

  Copyright (C) 2019 Magma Collective T&DT, License GNU AGPL v3+
  This is free software: you are free to change and redistribute it
  For the latest sourcecode go to <https://code.cobox.cloud/>

Usage: cobox <command> [options]

Commands:
  cobox folders <command>     view and interact with your folders
  cobox keys <command>        manage your app's keys
  cobox nickname <name>       change your public name
  cobox seeders <command>     send commands to a remote seeder
  cobox seeds <command>       seed your friend's folder on your local device
  cobox whoami                display your nickname and cobox key
  cobox setup-fuse [options]  configure fuse
  cobox start [options]       start cobox                         [aliases: up]
  cobox stop [options]        stop the app                      [aliases: down]

Options:
  -h, --help, --help        Show help                                  [boolean]
  -v, --version, --version  Show version number                        [boolean]

For more information on cobox read the manual: man cobox
Please report bugs on <http://gitlab.com/coboxcoop/core/issues>.
```

## API
See swagger documentation... (we won't have this for a while).

## Contributing
PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

[`AGPL-3.0-or-later`](./LICENSE)
