const maybe = require('call-me-maybe')
const fs = require('fs')
const os = require('os')
const path = require('path')
const constants = require('@coboxcoop/constants')
const yargs = require('yargs')
const YAML = require('js-yaml')
const config = require('@coboxcoop/config')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:main')
const COBOXRC = '.coboxrc'
const App = require('./index')
const options = require('./bin/lib/options')

const args = yargs
  .config('config', (configPath) => config.load(configPath || COBOXRC))
  .options(options)
  .argv

if (require.main === module) return run(args)
else module.exports = run

function run (opts, cb) {
  return maybe(cb, new Promise((resolve, reject) => {
    const app = App(opts)

    app.start((err) => {
      if (err) {
        debug({ msg: 'failed to start the app properly', err })
        throw err
      }
      var hostname = app.get('hostname')
      var port = app.get('port')
      var mount = app.get('mount')
      var storage = app.get('storage')
      var udpPort = app.get('udpPort')

      debug({ hostname, port, mount, storage, udpPort })

      if (isTruthy(opts.ui)) openUI()
      return resolve(app)

      function openUI() {
        require('open')(`http://${hostname}:${port}`)
      }
    })
  }))
}

function isTruthy (t) {
  return t && (t === 'true' || t === true)
}
